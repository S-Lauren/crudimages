package org.sid;

import java.nio.file.Path;
import java.nio.file.Paths;
import org.sid.dao.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrudimageApplication implements CommandLineRunner {

	@Autowired
	ProductRepository prod; 

	public static void main(String[] args) {
		SpringApplication.run(CrudimageApplication.class, args);
	}
	
	@Override
	public void run(String... args) throws Exception {
		
		 String filename="Iphone.jpg";
		    Path pathToFile = Paths.get(filename);
		    System.out.println(pathToFile.toAbsolutePath());
		
	}
}
