package org.sid.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;

import org.springframework.web.multipart.MultipartFile;

@Entity
public class Product implements Serializable {

	@Id 
	@GeneratedValue(strategy =GenerationType.AUTO)
	private Long id; 
	private String name; 
	private String description; 
	private double price; 
	private Date dateCreation; 
	private String image; 
	
	@Transient
	private List<MultipartFile> files = new ArrayList<MultipartFile>(); 


	@Transient
	private List<String> removeImages = new ArrayList<String>();
	

	public Product() {
		super();
		// TODO Auto-generated constructor stub
	}



	public Product(String name, String description, double price, Date dateCreation) {
		super();
		this.name = name;
		this.description = description;
		this.price = price;
		this.dateCreation = dateCreation;
	}



	public Long getId() {
		return id;
	}

	

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public Date getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}

	public List<MultipartFile> getFiles() {
		return files;
	}

	public void setFiles(List<MultipartFile> files) {
		this.files = files;
	}



	@Override
	public String toString() {
		return "Product [id=" + id + ", name=" + name + ", description=" + description + ", price=" + price
				+ ", dateCreation=" + dateCreation + ", files=" + files + ", removeImages=" + removeImages + "]";
	}



	public String getImage() {
		return image;
	}



	public void setImage(String image) {
		this.image = image;
	}
	
}
