package org.sid.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity
public class SecSecurityConfig extends WebSecurityConfigurerAdapter{

	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		// TODO Auto-generated method stub
		auth.inMemoryAuthentication().withUser("admin").password(passwordencoder().encode("1234")).roles("ADMIN");
//		auth.inMemoryAuthentication().withUser("user")
//		.password("{noop}a1234").roles("USER");
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
        http
        .formLogin()
        .loginPage("/login")
        .defaultSuccessUrl("/home")
        .and()
        .authorizeRequests().antMatchers("/home").hasRole("ADMIN"); 
    
        http.logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
        .logoutSuccessUrl("/login/logout.done").deleteCookies("JSESSIONID")
        .invalidateHttpSession(true) ;
	}
	
	@Bean
	public PasswordEncoder passwordencoder() {
		return new BCryptPasswordEncoder();
	}
}
