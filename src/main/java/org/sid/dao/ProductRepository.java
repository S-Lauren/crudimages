package org.sid.dao;

import org.sid.entities.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ProductRepository extends JpaRepository<Product, Long>{

	@Query("select p from Product p where p.id=:x")
	public Product findByProductId(@Param("x")Long id);

}
	