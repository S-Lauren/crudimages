package org.sid.web;

import java.util.List;

import javax.validation.Valid;

import org.sid.dao.ProductRepository;
import org.sid.entities.Product;

import org.sid.metier.ProductMetier;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;



@Controller
public class ProductController {

	@Autowired
	ProductRepository productrepository; 
	@Autowired
	ProductMetier productmetier; 

	
	@GetMapping("/home")
	public String getProduct(@ModelAttribute Product product,Long id,  Model model) {
		
		
		List<Product> products = productrepository.findAll(); 
	
		model.addAttribute("products", products); 
		model.addAttribute("prod", new Product()); 
		
		return "products"; 
		
	}
	
	@PostMapping("/save")
	public String saveProduct(Model model, Product p) {
		
		Product p1 = productmetier.productSave(p);
		model.addAttribute("prod", new Product());
		model.addAttribute("p1", p1);
		return "redirect:/home"; 
		
	}
	
	@GetMapping("/edit")
	public String editProduct( Long id, Model model) {

		Product p = productrepository.findById(id).get();		 	 
		model.addAttribute("product",p);
		model.addAttribute("prod", new Product());
		return "edit";
	}
	
	
	
	@GetMapping("/delete/{id}")
	public String removeProduct(@PathVariable Long id, Model model) {
		
		model.addAttribute("prod", new Product()); 
		Product p = productrepository.findByProductId(id);	
		productrepository.delete(p);	
		return "redirect:/home"; 
	}

	
	
}
