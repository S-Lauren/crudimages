package org.sid.metier;

import java.io.File;

import org.sid.entities.Product;

public interface ProductMetier {

	public File productPath(String path, String photo); 
	public Product productSave(Product p);

}
