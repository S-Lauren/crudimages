package org.sid.metier;

import java.io.File;
import javax.servlet.ServletContext;
import javax.transaction.Transactional;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import org.sid.dao.ProductRepository;
import org.sid.entities.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;


@Service
@Transactional
public class ProductMetierImpl implements ProductMetier {
	
	// Servlet context pour le path
	@Autowired
	private ServletContext ctx; 
	@Autowired
	private ProductRepository productrepository; 

	


	@Override
	public File productPath(String uploadedPhoto, String pathPhoto) {
		
		boolean exists = new File(ctx.getRealPath("/"+pathPhoto+"/")).exists();
		
		if(!exists) 
			 new File(ctx.getRealPath("/"+pathPhoto+"/")).mkdir();
		
			
		String newPath = ctx.getRealPath("/"+pathPhoto+"/"+uploadedPhoto);
		File file = new File(newPath);
		return file;
	}
	

	
	@Override
	public Product productSave(Product p) {
		
		Product p1 = productrepository.save(p);
		
		for(MultipartFile file : p.getFiles()) {
			
			String fileName = file.getOriginalFilename();
			String uploadedFile = FilenameUtils.getBaseName(fileName)+"."+FilenameUtils.getExtension(fileName);
			
			File pathFile = this.productPath(uploadedFile, "images");
			
			
			if(pathFile != null) {
				try {
					FileUtils.writeByteArrayToFile(pathFile, file.getBytes());
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
			p1.setImage(uploadedFile);
		
		}
		
		return p1;
	}

}
