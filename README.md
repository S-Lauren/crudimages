# WishListApp

Christmas is coming soon ! We need to organize our Gift Shop ! 

This is why I decided to make a little MVP application on wishList,
an easy way to remember what we suppose to buy, sometimes we don't remember clearly
where was this thing we saw last year or maybe two months ago because we didn't note the related link of the website
we found it. 

WishList will help you to remember ! 

You will be able to : 


- See your gifts's list
- Create your gift and save it in memory
- Upload your gift photo
- Edit your gift
- Delete your gift

Nice to have on next upgrade : 

- Register other users in table by jdbc authentication
- Create categorie of different list 
